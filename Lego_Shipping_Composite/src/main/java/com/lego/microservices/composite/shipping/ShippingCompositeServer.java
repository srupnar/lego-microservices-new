/**
 * 
 */
package com.lego.microservices.composite.shipping;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.client.RestTemplate;

import com.lego.microservices.composite.shipping.service.ShippingWebController;
import com.lego.microservices.composite.shipping.service.ShippingWebService;

/**
 * User web-server. Works as a microservice client, fetching data from the
 * Users-Service. Uses the Discovery Server (Eureka) to find the microservice.
 * 
 * @author Suresh.Rupnar
 * 
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class })
@EnableCircuitBreaker
@EnableDiscoveryClient
@EnableResourceServer
@ComponentScan({ "com.lego.microservices.composite.shipping" })
public class ShippingCompositeServer {

	private static final Logger LOG = LoggerFactory.getLogger(ShippingCompositeServer.class);

	static {
		// for localhost testing only
		LOG.warn("Will now disable hostname check in SSL, only to be used during development");
		HttpsURLConnection.setDefaultHostnameVerifier((hostname, sslSession) -> true);
	}

	/**
	 * URL uses the logical name of Shipping-service - upper or lower case,
	 * doesn't matter.
	 */
	public static final String SHIPPING_SERVICE_URL = "http://SHIPPING-SERVICE";

	public static void main(String[] args) {

		ConfigurableApplicationContext ctx = SpringApplication.run(ShippingCompositeServer.class, args);

		LOG.info("Connected to RabbitMQ at: {}", ctx.getEnvironment().getProperty("spring.rabbitmq.host"));
	}

	/**
	 * A customized RestTemplate that has the ribbon load balancer build in.
	 * Note that prior to the "Brixton"
	 * 
	 * @return
	 */
	@LoadBalanced
	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	/**
	 * The ShippingWebService encapsulates the interaction with the
	 * micro-service.
	 * 
	 * @return A new service instance.
	 */
	@Bean
	public ShippingWebService shippingWebService() {
		return new ShippingWebService(SHIPPING_SERVICE_URL);
	}

	/**
	 * Create the controller, passing it the {@link ShippingWebService} to use.
	 * 
	 * @return
	 */
	@Bean
	public ShippingWebController shippingWebController() {
		return new ShippingWebController(shippingWebService());
	}

}
