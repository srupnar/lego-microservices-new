/**
 * 
 */
package com.lego.microservices.core.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.lego.microservices.services.config.DatabaseConfig;

/**
 * Run as a micro-service, registering with the Discovery Server (Eureka).
 * <p>
 * Note that the configuration for this application is imported from
 * {@link DatabaseConfig}. This is a deliberate separation of concerns.
 * 
 * @author Suresh.Rupnar
 * 
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(DatabaseConfig.class)
@ComponentScan({"com.lego.microservices.core.users", "com.lego.microservices.services.util"})
public class UsersServer {

	private static final Logger LOG = LoggerFactory.getLogger(UsersServer.class);

	/**
	 * Run the application using Spring Boot and an embedded servlet engine.
	 * 
	 * @param args
	 *            Program arguments - ignored.
	 */	
	public static void main(String[] args) {

        ConfigurableApplicationContext ctx = SpringApplication.run(UsersServer.class, args);

        LOG.info("Connected to RabbitMQ at: {}", ctx.getEnvironment().getProperty("spring.rabbitmq.host"));
    }

}
