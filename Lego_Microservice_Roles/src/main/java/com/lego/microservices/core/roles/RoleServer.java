package com.lego.microservices.core.roles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import com.lego.microservices.services.config.DatabaseConfig;

/**
 * Run as a micro-service, registering with the Discovery Server (Eureka).
 * <p>
 * Note that the configuration for this application is imported from
 * {@link RoleConfiguration}. This is a deliberate separation of concerns.
 * 
 * @author katre.v
 * 
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(DatabaseConfig.class)
@ComponentScan({"com.lego.microservices.core.roles", "com.lego.microservices.services.util"})
public class RoleServer {
	
	private static final Logger LOG = LoggerFactory.getLogger(RoleServer.class);

	/**
	 * Run the application using Spring Boot and an embedded servlet engine.
	 * 
	 * @param args
	 *            Program arguments - ignored.
	 */
	public static void main(String[] args) {

		ConfigurableApplicationContext ctx = SpringApplication.run(RoleServer.class, args);

		LOG.info("Connected to RabbitMQ at: {}", ctx.getEnvironment().getProperty("spring.rabbitmq.host"));
	}

}
