/**
 * 
 */
package com.lego.microservices.composite.product.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * @author Suresh.Rupnar
 *
 */
@Configuration
public class MvcConfig extends WebMvcConfigurationSupport {

	@Bean
	public RequestMappingHandlerMapping requestMappingHandlerMapping() {
		RequestMappingHandlerMapping handlerMapping = super.requestMappingHandlerMapping();
		handlerMapping.setUseSuffixPatternMatch(false);
		handlerMapping.setUseRegisteredSuffixPatternMatch(true);
		handlerMapping.setUseTrailingSlashMatch(false);
		return handlerMapping;
	}

}
