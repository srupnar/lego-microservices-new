package com.lego.microservices.composite.product.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.lego.microservices.exceptions.GenericException;
import com.lego.microservices.msg.Messages;
import com.lego.microservices.persistence.Orders;
import com.lego.microservices.persistence.dto.ErrorDto;
import com.lego.microservices.persistence.dto.SearchParamDto;
import com.lego.microservices.persistence.dto.UpdateOrderDto;
import com.lego.microservices.services.util.ServiceUtils;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

/**
 * Client controller, fetches Order info from the microservice via
 * {@link OrderWebService}.
 * 
 * @author Suresh.Rupnar
 * 
 */
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RestController
public class OrderWebController {

	@Autowired
	protected OrderWebService orderWebService;

	@Autowired
	ServiceUtils util;

	protected Logger logger = LoggerFactory.getLogger(OrderWebController.class);

	/**
	 * Fetch list of all orders
	 * 
	 * @return
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/orders", produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<Object>> getAllOrders() {

		List<Object> orderDto = null;
		try {
			logger.info("OrderWeb-service getAllOrders() invoked");

			orderDto = orderWebService.getAllOrders();

			logger.info("OrderWeb-service getAllOrders() found");
		} catch (HttpClientErrorException e) {
			logger.info("Error : " + e);
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),
					Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
			List<Object> errorlist = new ArrayList<Object>();
			errorlist.add(errorDto);
			return util.createNotOkResponse(errorlist);
		}

		return util.createOkResponse(orderDto);
	}

	/**
	 * Fetch list of all orders filtered by Role Attribute name and value
	 * 
	 * @return
	 */
	// @CrossOrigin(origins = "http://localhost:8090")
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/orders", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public List<Object> getOrders(@RequestBody SearchParamDto searchParamDto) {

		if (StringUtils.isEmpty(searchParamDto.getRoleAttrName())
				|| StringUtils.isEmpty(searchParamDto.getRoleAttrValue())) {
			throw new GenericException("Required input parameter's are missing", null);
		}

		String roleAttrName = searchParamDto.getRoleAttrName();
		String roleAttrValue = searchParamDto.getRoleAttrValue();
		
		List<Object> orderDto = null;
		try {
			logger.info("OrderWeb-service getOrders(String roleAttrName,String roleAttrValue) invoked");

			if (roleAttrName.equalsIgnoreCase("admin") && roleAttrValue.equalsIgnoreCase("admin")) {
				orderDto = orderWebService.getAllOrders();
			} else {
				orderDto = orderWebService.getOrders(roleAttrName, roleAttrValue);
			}
			logger.info("OrderWeb-service getOrders(String roleAttrName,String roleAttrValue) found");
		} catch (HttpClientErrorException e) {
			logger.info("Error : " + e);
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),
					Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
			List<Object> errorlist = new ArrayList<Object>();
			errorlist.add(errorDto);
			return errorlist;
		}

		return orderDto;
	}

	/**
	 * Fetch order details by order ID
	 * 
	 * @return
	 */
	// @CrossOrigin(origins = "http://localhost:8090")
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.GET, path = "/orders/{orderId}", produces = MediaType.APPLICATION_JSON)
	public Object getOrderById(@PathVariable("orderId") long orderId) {

		Object orderDto = null;
		try {
			logger.info("OrderWeb-service getOrderById(long orderId) invoked");

			orderDto = orderWebService.getOrderById(orderId);
			logger.info("OrderWeb-service getOrderById(long orderId) found");
		} catch (HttpClientErrorException e) {
			logger.info("Error : " + e);
			return new ErrorDto(Messages.getMessage("errorCode_BlankUIDPSWD"),
					Messages.getMessage("errorCodeMessage_BlankUIDPSWD"));
		}

		return orderDto;
	}

	/**
	 * Fetch list of all orders basis on orderId, mouldNo and artNo
	 * 
	 * @return List<Object>, if order found. Else error message
	 */
	// @CrossOrigin(origins = "http://localhost:8090")
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/search", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public Object getGenericSearch(@RequestBody SearchParamDto searchParamDto) {

		if (StringUtils.isEmpty(searchParamDto.getRoleAttrName())
				|| StringUtils.isEmpty(searchParamDto.getRoleAttrValue())
				|| StringUtils.isEmpty(searchParamDto.getGenericAttribute())) {
			throw new GenericException("Required input parameter's are missing", null);
		}

		String roleAttrName = searchParamDto.getRoleAttrName();
		String roleAttrValue = searchParamDto.getRoleAttrValue();
		String genericAttribute = searchParamDto.getGenericAttribute();

		Object orderDto = null;
		try {
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) invoked");

			orderDto = orderWebService.getGenericSearch(roleAttrName, roleAttrValue, genericAttribute);
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) found");
		} catch (HttpClientErrorException e) {
			logger.info("Error : " + e);
			return new ErrorDto(Messages.getMessage("errorCode_TechnicalError"),
					Messages.getMessage("errorMessage_TechnicalError") + e.getMessage());
		}

		return orderDto;
	}

	/**
	 * Create Order based on provided JSON data for order creation
	 * 
	 * @param order,
	 *            JSON object
	 * @return Order creation message. Else error message
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/createorder", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public Object createOrder(@RequestBody Orders order) {

		Object orderDto = null;
		try {
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) invoked");

			orderDto = orderWebService.createOrder(order);
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) found");
		} catch (HttpClientErrorException e) {
			logger.info("Error : " + e);
			return new ErrorDto(Messages.getMessage("errorCode_TechnicalError"),
					Messages.getMessage("errorMessage_TechnicalError") + e.getMessage());
		}

		return orderDto;
	}

	/**
	 * 
	 * @param orderstatus
	 * @return
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/updateorder", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public Object updateOrder(@RequestBody UpdateOrderDto updateOrderDto) {

		Object orderUpdateDto = null;
		try {
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) invoked");

			orderUpdateDto = orderWebService.updateOrder(updateOrderDto);
			logger.info("OrderWeb-service getGenericSearch(String genericAttribute) found");
		} catch (HttpClientErrorException e) {
			logger.info("OrderWeb-service getAllOrders() exception : " + e);
			return new ErrorDto(Messages.getMessage("errorCode_TechnicalError"),
					Messages.getMessage("errorMessage_TechnicalError") + e.getMessage());
		}
		return orderUpdateDto;
	}

	/**
	 * 
	 * @param order
	 * @return
	 */
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/updateshipping", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public Object updateShippingOrder(@RequestBody Orders order) {

		Object shippingOrderDto = null;
		try {
			logger.info("OrderWeb-service updateShippingOrder(Orders order) invoked");

			shippingOrderDto = orderWebService.updateShippingOrder(order);
			logger.info("OrderWeb-service updateShippingOrder(Orders order) found");
		} catch (HttpClientErrorException e) {
			logger.info("OrderWeb-service updateShippingOrder() exception : " + e);
			return new ErrorDto(Messages.getMessage("errorCode_TechnicalError"),
					Messages.getMessage("errorMessage_TechnicalError") + e.getMessage());
		}

		return shippingOrderDto;
	}

	/**
	 * Fetch Weekly/Monthly order status count
	 * 
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@CrossOrigin()
	@RequestMapping(method = RequestMethod.POST, path = "/getStatistics", produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
	public List<Object> getStatistics(@RequestBody SearchParamDto searchParamDto) {

		if (StringUtils.isEmpty(searchParamDto.getRoleAttrName())
				|| StringUtils.isEmpty(searchParamDto.getRoleAttrValue())) {
			throw new GenericException("Required input parameter's are missing", null);
		}

		String roleAttrName = searchParamDto.getRoleAttrName();
		String roleAttrValue = searchParamDto.getRoleAttrValue();

		List<Object> staticsDtoMain = null;
		try {
			logger.info("OrderWeb-service getStatistics() invoked");

			staticsDtoMain = orderWebService.getStatistics(roleAttrName, roleAttrValue);

			logger.info("OrderWeb-service getStatistics() found");
		} catch (HttpClientErrorException e) {
			logger.info("OrderWeb-service getStatistics() exception : " + e);
			ErrorDto errorDto = new ErrorDto(Messages.getMessage("errorCode_TechnicalError"),
					Messages.getMessage("errorMessage_TechnicalError"));
			List<Object> errorlist = new ArrayList();
			errorlist.add(errorDto);
			return errorlist;
		}
		return staticsDtoMain;
	}

}
