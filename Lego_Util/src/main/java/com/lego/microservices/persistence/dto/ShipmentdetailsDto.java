/**
 * 
 */
package com.lego.microservices.persistence.dto;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 * @author katre.v
 *
 */
@JsonRootName("Shipmentdetail")
public class ShipmentdetailsDto {

	private Integer trackingId;
	private String shippingAgency;
	private String lastStatus;
	private String updatedDate;

	private LinedetailsDto linedetailsDto;
	private OrderDto orderDto;

	/**
	 * default contructor
	 */
	public ShipmentdetailsDto() {
	}

	public Integer getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(Integer trackingId) {
		this.trackingId = trackingId;
	}

	public LinedetailsDto getLinedetailsDto() {
		return linedetailsDto;
	}

	public void setLinedetailsDto(LinedetailsDto linedetailsDto) {
		this.linedetailsDto = linedetailsDto;
	}

	public OrderDto getOrderDto() {
		return orderDto;
	}

	public void setOrderDto(OrderDto orderDto) {
		this.orderDto = orderDto;
	}

	public String getShippingAgency() {
		return shippingAgency;
	}

	public void setShippingAgency(String shippingAgency) {
		this.shippingAgency = shippingAgency;
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;

	}

}
