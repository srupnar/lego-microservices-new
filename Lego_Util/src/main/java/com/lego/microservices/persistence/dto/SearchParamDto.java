/**
 * 
 */
package com.lego.microservices.persistence.dto;

/**
 * @author Suresh.Rupnar
 *
 */
public class SearchParamDto {

	String roleAttrName;
	String roleAttrValue;
	String genericAttribute;

	/**
	 * @return the roleAttrName
	 */
	public String getRoleAttrName() {
		return roleAttrName;
	}

	/**
	 * @param roleAttrName
	 *            the roleAttrName to set
	 */
	public void setRoleAttrName(String roleAttrName) {
		this.roleAttrName = roleAttrName;
	}

	/**
	 * @return the roleAttrValue
	 */
	public String getRoleAttrValue() {
		return roleAttrValue;
	}

	/**
	 * @param roleAttrValue
	 *            the roleAttrValue to set
	 */
	public void setRoleAttrValue(String roleAttrValue) {
		this.roleAttrValue = roleAttrValue;
	}

	/**
	 * @return the genericAttribute
	 */
	public String getGenericAttribute() {
		return genericAttribute;
	}

	/**
	 * @param genericAttribute
	 *            the genericAttribute to set
	 */
	public void setGenericAttribute(String genericAttribute) {
		this.genericAttribute = genericAttribute;
	}

}
