/**
 * 
 */
package com.lego.microservices.persistence.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.lego.microservices.persistence.Orders;

/**
 * Repository for Order persistent entity
 * @author Suresh.Rupnar
 *
 */
public interface OrderRepository extends Repository<Orders, Long> {
	
	/**
	 * fetch all orders
	 * @return List<Orders>
	 */
	public List<Orders> findAll();
	
	/**
	 * fetch list of orders by POStatus 
	 * @param postatus
	 * @return List<Orders>
	 */
	public List<Orders> findBypostatus(String postatus);
	
	/**
	 * fetch list of orders by Updated By
	 * @param updatedBy
	 * @return List<Orders>
	 */
	public List<Orders> findByupdatedBy(String updatedBy);
	
	/**
	 * fetch list of orders by Created By
	 * @param createdBy
	 * @return List<Orders>
	 */
	public List<Orders> findBycreatedBy(String createdBy);
	
	/**
	 * fetch list of orders by Warehouse city
	 * @param warehouseCity
	 * @return List<Orders>
	 */
	public List<Orders> findBywarehouseCity(String warehouseCity);
	
	/**
	 * fetch list of orders by Supplier Addresse1
	 * @param supplierAddress1
	 * @return List<Orders>
	 */
	public List<Orders> findBysupplierAddress1(String supplierAddress1);
	
	/**
	 * fetch list of orders by Supplier Addresse2
	 * @param supplierAddress2
	 * @return List<Orders>
	 */
	public List<Orders> findBysupplierAddress2(String supplierAddress2);
	
	/**
	 * fetch list of orders by Warehouse Addresse1
	 * @param warehouseAddress1
	 * @return List<Orders>
	 */
	public List<Orders> findBywarehouseAddress1(String warehouseAddress1);
	
	/**
	 * fetch list of orders by Warehouse Address2
	 * @param warehouseAddress2
	 * @return List<Orders>
	 */
	public List<Orders> findBywarehouseAddress2(String warehouseAddress2);
	
	/**
	 * fetch list of orders by Warehouse Country
	 * @param warehouseCountry
	 * @return List<Orders>
	 */
	public List<Orders> findBywarehouseCountry(String warehouseCountry);
	
	/**
	 * fetch list of orders by Supplier Id
	 * @param supplierId
	 * @return List<Orders>
	 */
	public List<Orders> findBysupplierId(Integer supplierId);
	
	/**
	 * fetch list of orders by Supplier Country
	 * @param supplierCountry
	 * @return List<Orders>
	 */
	public List<Orders> findBysupplierCountry(String supplierCountry);
	
	/**
	 * fetch list of orders by Broker ID
	 * @param brokerId
	 * @return List<Orders>
	 */
	public List<Orders> findBybrokerId(String brokerId);

	/**
	 * fetch order details by orderId
	 * 
	 * @param orderId
	 * @return
	 */
	public Orders findByOrderId(Long orderId);

}
